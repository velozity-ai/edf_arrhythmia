#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import glob
import os.path
import pandas as pd
import numpy as np

folder_path = "path/to/the/uploads"
file_type = '\*edf'
files = glob.glob(folder_path + file_type)
max_file = max(files, key=os.path.getctime)


# In[102]:


import numpy as np
import mne
edf = mne.io.read_raw_edf('10-07-48.EDF')
header = ','.join(edf.ch_names)
k = np.savetxt('10-07-48-bh.csv', edf.get_data().T, delimiter=',', header=header)


# In[104]:


df = pd.read_csv("C:\\Users\\dhara\\10-07-48-bh.csv")


# In[105]:


df


# In[1]:


import pandas as pd
import numpy as np
from sklearn import preprocessing
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split


# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')


# In[3]:


data = pd.read_csv(r"C:\Users\sweta\Desktop\EDF\dataset2\demo\demo.csv")


# In[4]:


data


# In[6]:


data_encoded = data
encoder = preprocessing.LabelEncoder()
data_encoded['encoded'] = encoder.fit_transform(data_encoded['classification_data'])
data_encoded


# In[7]:


data_encoded


# In[8]:


data_encoded.dtypes[data_encoded.dtypes ==object]


# In[10]:


data_encoded


# In[11]:


x = data_encoded.drop(['request_id','classification_data','encoded'],axis='columns')


# In[12]:


x


# In[14]:


x1 = x['Ecg-data'].tolist()


# In[15]:


y = data_encoded.encoded


# In[16]:


y


# In[17]:


x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.2)


# In[18]:


x_train


# In[19]:


y_train


# In[20]:


x_test


# In[21]:


y_test


# In[22]:


from sklearn.svm import SVC


# In[23]:


model = SVC()


# In[24]:


model.fit(x_train,y_train)


# In[25]:


model.score(x_test,y_test)


# In[35]:


import pickle
filename = r"C:\\Users\sweta\Desktop\EDF\dataset2\Data\Clean\test.sav"
pickle.dump(model, open(filename, 'wb'))


# In[26]:


preds = model.predict(x_test)
preds


# In[27]:


def mod_pred(x_test,model):
    preds = model.predict(x_test)
    preds_arr = []
    for i in preds:
        if i == 0:
            preds_arr.append("2nd Degree AVB Type l")
        elif i == 1:
            preds_arr.append("3rd Degree AVB")
        elif i == 2:
            preds_arr.append("Artifact/Noise")
        elif i == 3:
            preds_arr.append("Atrial fibrillation")
        elif i == 4:
            preds_arr.append("Atrial runs(3-5 PACs)")
        elif i == 5:
            preds_arr.append("Inconclusive")
        elif i == 6:
            preds_arr.append("Junctional Rhythm")
        elif i == 7:
            preds_arr.append("Multifocal PVCs/couplet")
        elif i == 8:
            preds_arr.append("PSVT(sudden onset and offset of SVT)(>5 PACs)")
        elif i == 9:
            preds_arr.append("Pause")
        elif i == 10:
            preds_arr.append("Sinus Rhythm")
        elif i == 11:
            preds_arr.append("Sinus Tachycardia")
        elif i == 12:
            preds_arr.append("Ventricular Escape Beats")
        elif i == 13:
            preds_arr.append("Ventricular Pre Excitation")
        else:
            preds_arr.append("Ventricular Tachycardia")
    return preds_arr


# In[28]:


model1 = mod_pred(x_test,model)


# In[29]:


x_test['Arrhythmia'] = model1
x_test['tag_id'] = y_test
#x_test.drop(columns=['Predictions'],axis=1,inplace=True)
x_test


# In[30]:


x_test.to_csv('r'C:\\Users\\dhara\final_data.csv', index = False)


# In[34]:


import pickle
filename = r"C:\\Users\sweta\Desktop\EDF\dataset2\Data\Clean\demo.sav"
pickle.dump(model, open(filename, 'wb'))


# In[ ]:




