import pandas as pd
from sklearn import preprocessing
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
import numpy as np
import mne 


def edf_csv(edf_file_path):
    edf = mne.io.read_raw_edf(edf_file_path)
    header = ','.join(edf.ch_names)
    k = np.savetxt('jump2.csv', edf.get_data().T, delimiter=',', header=header)  
    return "jump2.csv"

def mod_pred(x_test,model):
    preds = model.predict(x_test)
    preds_arr = []
    for i in preds:
        if i == 0:
            preds_arr.append("2nd Degree AVB Type l")
        elif i == 1:
            preds_arr.append("3rd Degree AVB")
        elif i == 2:
            preds_arr.append("Artifact/Noise")
        elif i == 3:
            preds_arr.append("Atrial fibrillation")
        elif i == 4:
            preds_arr.append("Atrial runs(3-5 PACs)")
        elif i == 5:
            preds_arr.append("Inconclusive")
        elif i == 6:
            preds_arr.append("Junctional Rhythm")
        elif i == 7:
            preds_arr.append("Multifocal PVCs/couplet")
        elif i == 8:
            preds_arr.append("PSVT(sudden onset and offset of SVT)(>5 PACs)")
        elif i == 9:
            preds_arr.append("Pause")
        elif i == 10:
            preds_arr.append("Sinus Rhythm")
        elif i == 11:
            preds_arr.append("Sinus Tachycardia")
        elif i == 12:
            preds_arr.append("Ventricular Escape Beats")
        elif i == 13:
            preds_arr.append("Ventricular Pre Excitation")
        else:
            preds_arr.append("Ventricular Tachycardia")
    return preds_arr



def train(file):
    data = pd.read_csv(file)

    data['classification_data'].unique()
    data_encoded = data
    encoder = preprocessing.LabelEncoder()
    data_encoded['encoded'] = encoder.fit_transform(data_encoded['classification_data'])
    data_encoded.dtypes[data_encoded.dtypes ==object]
    data_encoded['Unnamed: 1250'] = [float(elem.replace(']','')) for elem in data_encoded['Unnamed: 1250']]
    data_encoded['ecg_data'] = [float(elem.replace('[','')) for elem in data_encoded['ecg_data']]

    x = data_encoded.drop(['request_id','classification_data','encoded'],axis='columns')
    x1 = x['ecg_data'].tolist()
    y = data_encoded.encoded
    x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.030)

    clf = RandomForestClassifier(n_estimators = 300)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)

    print("ACCURACY OF THE MODEL: ", metrics.accuracy_score(y_test, y_pred))

    preds = clf.predict(x_test)
    model1 = mod_pred(x_test,clf)
    x_test['Arrhythmia'] = model1
    x_test['tag_id'] = y_test
    data3 = x_test

    df = pd.DataFrame(data3)

    print (df)
    df.to_csv(r'C:\\Users\\dhara\File Name.csv', index = False)
    return 'C:\\Users\\dhara\File Name.csv'










