from fastapi import FastAPI
import numpy as np
from pydantic import BaseModel
from totalmodel import edf_csv,train


my_app = FastAPI()


class Info(BaseModel):
    
    edf_file_path : str



@my_app.post("/getEDF/Files")
def getInformation(info : dict):
    print(info)
    csv = edf_csv(info['edf_file_path'])
    trainedFile = train(csv)
    return {

        "status" : "SUCCESS",
        "data" : [],
        "trainedFile" : trainedFile
    }

