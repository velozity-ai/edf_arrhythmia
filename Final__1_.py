#!/usr/bin/env python
# coding: utf-8

# In[4]:


import glob
import os.path
import mne
import pandas as pd
import numpy as np
import pickle
from sklearn import model_selection

folder_path = "C:/Users/dhara/Downloads/"
file_type = '\*edf'
files = glob.glob(folder_path + file_type)
max_file = max(files, key=os.path.getctime)
saved_csv = os.path.splitext(max_file)[0] + '.csv'
final_csv = os.path.splitext(max_file)[0] + '-final.csv'
print(max_file)
print(saved_csv)
print(final_csv)


# In[5]:


edf = mne.io.read_raw_edf(max_file)
header = ','.join(edf.ch_names)
Saved = np.savetxt(saved_csv, edf.get_data().T, delimiter=',', header=header)
print("Done!")


# In[6]:


model = "C:/Users/dhara/Downloads/test.sav"
load_model = pickle.load(open(model, 'rb'))
data = pd.read_csv(saved_csv)
data


# In[7]:


test = data.drop(['Marker'],axis='columns')
test


# In[ ]:


result = load_model.predict(test)
result


# In[ ]:


data['tag_id'] = result.tolist()
data.to_csv(final_csv)
data


# In[ ]:




